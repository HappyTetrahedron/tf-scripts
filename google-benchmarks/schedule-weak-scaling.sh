#!/bin/bash

for i in $1
do
  echo "Scheduling job for ${i%,*} nodes ( ${i#*,} ps): "
for net in  $2
  do 
    echo -n "  $net... "
    sbatch -N ${i%,*}  google-benchmarks_dist_daint.sh ${i%,*}  ${i#*,}  parameter_server true $net 32
  done
done
